<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ARObject extends Model
{
    //
    protected $fillable = ['uid', 'title', 'link'];
}
